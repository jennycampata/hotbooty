<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@schedule');
Route::get('/schedule', 'HomeController@schedule')->name('schedule');
Route::get('/classes', 'HomeController@classes')->name('classes');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/booking', 'HomeController@booking')->name('subscribe');
//Route::post('/payment', 'PaymentController@payWithpaypal')->name('paywithpaypal');
Route::get('/status', 'PaymentController@getPaymentStatus')->name('status');
Route::get('/profile', 'ProfileController@index')->name('status');

//stripe payment
Route::post('/payment', 'StripeController@stripe')->name('payment');
Route::post('/stripe', 'StripeController@stripePost')->name('form');
Route::get('/logout', 'HomeController@logout')->name('logout');