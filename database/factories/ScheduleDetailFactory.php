<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ScheduleDetail;
use Faker\Generator as Faker;

$factory->define(ScheduleDetail::class, function (Faker $faker) {

    return [
        'schedule_id' => $faker->word,
        'description' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
