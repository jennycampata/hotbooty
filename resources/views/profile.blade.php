@include('templates/header')
<style>

    section.et_pb_module.et_pb_fullwidth_header.et_pb_fullwidth_header_0.et_pb_bg_layout_dark.et_pb_text_align_center.et_pb_fullscreen {
        background-image: url(/public/images/02-23.jpeg) !important;
    }

    .et_pb_row_2 {
        width: 85% !important;
        max-width: 2560px;
    }

    .introduction-s {
        background-image: linear-gradient(180deg, #00000075 0%, #0000002b 100%), url(public/images/09-IMG_6593-copy-400x284.jpg);
        background-size: cover;
        padding-top: 20%;
        padding-bottom: 10%;
    }

    .pagination {
        clear: both;
        display: inline-flex;
        color: #fc1564 !important;
    }

    .pagination > li > a {

        color: #ea196a !important;

    }

    .pagination > .active > a {
        background-color: #000000;
        border-color: #fc1564;
    }

    .introduction-s h1 {

        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        font-weight: 300;
        text-transform: uppercase;
        font-size: 80px;
        text-align: center;
        text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4);
        color: white;
    }

    .introduction-s span {
        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        text-transform: uppercase;
        font-size: 24px;
        line-height: 1.5em;
        text-align: center;
        color: white;
        text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4);
        padding-bottom: 10%;
    }

    .row.instruc img {
        height: 175px;
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

    button.btn.btn-lg.btn-success {
        background: #ea196a;
    }

    button.btn.btn-sm.btn-success {
        background: black !important;
    }
</style>

<div id="main-content">

    <div class="introduction-s">
        <h1 class="et_pb_module_header">PROFILE</h1>
        <span class="et_pb_fullwidth_header_subhead"></span>
    </div>

    <div class="container bootstrap snippet">
        <div class="row">
            <div class="col-sm-8"><h1>User name</h1>


            </div>

            <div class="col-sm-4">
                <a href="/users" class="pull-right"><img title="profile image" class="img-circle img-responsive"
                                                         src="images/08-HBB-logo-black-4.png"></a>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                    <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail"
                         alt="avatar">
                    {{--                    <h6>Upload a different photo...</h6>--}}
                    {{--                    <input type="file" class="text-center center-block file-upload">--}}
                </div>
                </hr><br>


                <ul class="list-group">
                    <li class="list-group-item text-muted">Class Information <i class="fa fa-dashboard fa-1x"></i></li>
                    <li class="list-group-item text-right">
                        @if(isset($total))
                        <span
                                class="pull-left"><strong>Available Class</strong></span> {{$total->total}}
                    </li>
                    @endif
                    <li class="list-group-item text-right"><span
                                class="pull-left"><strong>Classes Attended</strong></span> {{$booking}}
                    </li>

                </ul>

                {{--                <div class="panel panel-default">--}}
                {{--                    <div class="panel-heading">Social Media</div>--}}
                {{--                    <div class="panel-body">--}}
                {{--                        <i class="fa fa-facebook fa-2x"></i> <i class="fa fa-github fa-2x"></i> <i--}}
                {{--                                class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i> <i--}}
                {{--                                class="fa fa-google-plus fa-2x"></i>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                @if(\Illuminate\Support\Facades\Auth::user())
                    <li>
                        <a href="{{route('status')}}">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                    </li>
                    <li>
                        <a href="{{route('logout')}}">Logout</a>
                    </li>
                @else
                    <li><a href="{{route('login')}}">Login</a></li>
                    {{--<li><a href="{{route('registeruser')}}">Register</a></li>--}}
                @endif()


            </div><!--/col-3-->
            <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#messages"> Upcoming Class</a></li>
                    <li><a data-toggle="tab" href="#settings">Previous Class</a></li>
                </ul>


                <div class="tab-content">
                    {{--                    <div class="tab-pane active" id="home">--}}
                    {{--                        <hr>--}}
                    {{--                        <form class="form" action="##" method="post" id="registrationForm">--}}
                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="first_name"><h4>First name</h4></label>--}}
                    {{--                                    <input type="text" class="form-control" name="first_name" id="first_name"--}}
                    {{--                                           placeholder="first name" title="enter your first name if any.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="last_name"><h4>Last name</h4></label>--}}
                    {{--                                    <input type="text" class="form-control" name="last_name" id="last_name"--}}
                    {{--                                           placeholder="last name" title="enter your last name if any.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}

                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="phone"><h4>Phone</h4></label>--}}
                    {{--                                    <input type="text" class="form-control" name="phone" id="phone"--}}
                    {{--                                           placeholder="enter phone" title="enter your phone number if any.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}

                    {{--                            <div class="form-group">--}}
                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="mobile"><h4>Mobile</h4></label>--}}
                    {{--                                    <input type="text" class="form-control" name="mobile" id="mobile"--}}
                    {{--                                           placeholder="enter mobile number" title="enter your mobile number if any.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="email"><h4>Email</h4></label>--}}
                    {{--                                    <input type="email" class="form-control" name="email" id="email"--}}
                    {{--                                           placeholder="you@email.com" title="enter your email.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="email"><h4>Location</h4></label>--}}
                    {{--                                    <input type="email" class="form-control" id="location" placeholder="somewhere"--}}
                    {{--                                           title="enter a location">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="password"><h4>Password</h4></label>--}}
                    {{--                                    <input type="password" class="form-control" name="password" id="password"--}}
                    {{--                                           placeholder="password" title="enter your password.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="form-group">--}}

                    {{--                                <div class="col-xs-6">--}}
                    {{--                                    <label for="password2"><h4>Verify</h4></label>--}}
                    {{--                                    <input type="password" class="form-control" name="password2" id="password2"--}}
                    {{--                                           placeholder="password2" title="enter your password2.">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div class="form-group">--}}
                    {{--                                <div class="col-xs-12">--}}
                    {{--                                    <br>--}}
                    {{--                                    <button class="btn btn-lg btn-success" type="submit"><i--}}
                    {{--                                                class="glyphicon glyphicon-ok-sign"></i> Save--}}
                    {{--                                    </button>--}}
                    {{--                                    <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i>--}}
                    {{--                                        Reset--}}
                    {{--                                    </button>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </form>--}}

                    {{--                        <hr>--}}

                    {{--                    </div><!--/tab-pane-->--}}
                    <div class="tab-pane active " id="messages">

                        <h2></h2>

                        <hr>
                        <div class="card">
                            <div class="card-body">
                                <!-- Grid row -->
                                <div class="row">
                                    <!-- Grid column -->
                                    <div class="col-md-12">
                                    </div>
                                    <!-- Grid column -->
                                </div>
                                <!-- Grid row -->
                                <!--Table-->
                                <table class="table table-hover table-responsive mb-0">
                                    <!--Table head-->
                                    <thead>
                                    <tr>
                                        <th class="th-lg"><a>DATE</a></th>
                                        <th class="th-lg"><a>DAY</a></th>
                                        <th class="th-lg"><a href="">CLASS TYPE</a></th>
                                        <th class="th-lg"><a href="">TIME</a></th>
                                        <th class="th-lg"><a href="">EQUIPMENT</a></th>
                                        <th class="th-lg"><a href="">INSTRUCTOR</a></th>
                                        <th class="th-lg"><a href="">ACTION</a></th>

                                    </tr>
                                    </thead>
                                    <!--Table head-->
                                    <!--Table body-->
                                    <tbody>
                                    @foreach($schedule as $row)
                                        <tr>
                                            <td>{{$row->date}}</td>
                                            <td>@if($row->day == 0) Monday
                                                @elseif($row->day == 1) Tuesday
                                                @elseif($row->day == 2) Wednesday
                                                @elseif($row->day == 3) Thursday
                                                @elseif($row->day == 4) Friday
                                                @elseif($row->day == 5) Saturday
                                                @else Sunday @endif</td>
                                            <td>{{$row->date}}</td>
                                            <td>{{$row->date}}</td>
                                            <td>{{$row->date}}</td>
                                            <td>{{$row->date}}</td>
                                            <td>
                                                <button class="btn btn-success" type="submit"><i
                                                            class="glyphicon glyphicon-eye-open"></i> View
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach()
                                    </tbody>
                                    <!--Table body-->
                                </table>
                                <!--Bottom Table UI-->

                                <!--Bottom Table UI-->
                            </div>
                        </div>


                        <!--/tab-pane-->
                        <div class="tab-pane" id="settings">
                            <hr>
                            <div class="card">
                                <div class="card-body">
                                    <!-- Grid row -->
                                    <div class="row">
                                        <!-- Grid column -->
                                        <div class="col-md-12">
                                        </div>
                                        <!-- Grid column -->
                                    </div>
                                    <!-- Grid row -->
                                    <!--Table-->
                                    <table class="table table-hover table-responsive mb-0">
                                        <!--Table head-->
                                        <thead>
                                        <tr>
                                            <th class="th-lg"><a>DATE</a></th>
                                            <th class="th-lg"><a>DAY</a></th>
                                            <th class="th-lg"><a href="">CLASS TYPE</a></th>
                                            <th class="th-lg"><a href="">TIME</a></th>
                                            <th class="th-lg"><a href="">EQUIPMENT</a></th>
                                            <th class="th-lg"><a href="">INSTRUCTOR</a></th>
                                            <th class="th-lg"><a href="">ACTION</a></th>

                                        </tr>
                                        </thead>
                                        <!--Table head-->
                                        <!--Table body-->
                                        <tbody>
                                        @foreach($pschedule as $row)
                                            <tr>
                                                <td>{{$row->date}}</td>
                                                <td>@if($row->day == 0) Monday
                                                    @elseif($row->day == 1) Tuesday
                                                    @elseif($row->day == 2) Wednesday
                                                    @elseif($row->day == 3) Thursday
                                                    @elseif($row->day == 4) Friday
                                                    @elseif($row->day == 5) Saturday
                                                    @else Sunday @endif</td>
                                                <td>{{$row->date}}</td>
                                                <td>{{$row->date}}</td>
                                                <td>{{$row->date}}</td>
                                                <td>{{$row->date}}</td>
                                                <td>
                                                    <button class="btn btn-success" type="submit"><i
                                                                class="glyphicon glyphicon-eye-open"></i> View
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach()
                                        </tbody>
                                        <!--Table body-->
                                    </table>
                                    <!--Bottom Table UI-->

                                    <!--Bottom Table UI-->
                                </div>

                            </div>

                        </div><!--/tab-pane-->
                    </div><!--/tab-content-->

                </div><!--/col-9-->
            </div><!--/row-->
            <!-- .et_pb_post --></div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
                integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
                crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
                integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
                integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
                crossorigin="anonymous"></script>
@include('templates/footer')