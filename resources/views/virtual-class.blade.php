@include('templates/header')
<style>

    section.et_pb_module.et_pb_fullwidth_header.et_pb_fullwidth_header_0.et_pb_bg_layout_dark.et_pb_text_align_center.et_pb_fullscreen {
        background-image: url(/public/images/02-23.jpeg) !important;
    }

    .et_pb_pricing {
        text-align: center;
    }

    .et_pb_row_2 {
        width: 85% !important;
        max-width: 2560px;
    }

    div.et_pb_section.et_pb_section_1 {
        background-image: url(https://www.hotbootyballet.com/wp-content/uploads/2020/06/website-Virtual-classes.jpg) !important;
    }

    .introduction-s {
        background-image: linear-gradient(180deg, #00000075 0%, #0000002b 100%), url(public/images/website-Virtual-classes.jpg);
        background-size: cover;
        padding-top: 20%;
        padding-bottom: 10%;
    }

    .introduction-s h1 {

        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        font-weight: 300;
        text-transform: uppercase;
        font-size: 80px;
        text-align: center;
        text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4);
        color: white;
    }

    .introduction-s span {
        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        text-transform: uppercase;
        font-size: 24px;
        line-height: 1.5em;
        text-align: center;
        color: white;
        text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4);
        padding-bottom: 10%;
    }

    .row.instruc img {
        height: 175px;
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

    .row.instruc .col-md-2 {
        width: 19.666667%;
    }

    .row.instruc h1 {
        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        font-variant: small-caps;
        font-size: 5vw;
        color: #fc1564 !important;
        letter-spacing: 0em;
        line-height: 1.2em;
        text-align: center;
    }

    body .et_pb_button {
        border-color: #fc156400;

    }
</style>

<div id="main-content">

    <div class="introduction-s">
        <h1 class="et_pb_module_header">Virtual classes</h1>
        <span class="et_pb_fullwidth_header_subhead">Fitness and workout routines especially designed for the Hot Booty Babes in mind</span>
    </div>

    <article id="post-52454" class="post-52454 page type-page status-publish hentry">
        <div class="entry-content">


        </div> <!-- .et_pb_section -->
        <div class="et_pb_section et_pb_section_2 et_section_regular">


            <div class="et_pb_row et_pb_row_1">
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">


                    <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


                        <div class="et_pb_text_inner"><h3 style="text-align:center;">select a VIRTUAL CLASS PACKAGES&nbsp;</h3>
                        </div>
                    </div> <!-- .et_pb_text -->
                    <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">


                        <div class="et_pb_text_inner"><p style="text-align:center;">HOT BOOTY BALLET virtual classes are
                                presented using the Zoom app that is easily downloaded via your App Store or web
                                browser. To attend any of HBB classes, sign up via www.hotbootyballet.com website. You
                                can purchase individual classes or a 10 class bundle and choose any virtual classes on
                                the schedule.</p></div>
                    </div> <!-- .et_pb_text -->
                </div> <!-- .et_pb_column -->


            </div> <!-- .et_pb_row -->
            <div class="et_pb_row et_pb_row_2">
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_2  et_pb_css_mix_blend_mode_passthrough et-last-child">


                    <div class="et_pb_module et_pb_pricing_tables_0 et_pb_pricing clearfix et_pb_pricing_3 et_pb_pricing_no_bullet">


                        <div class="et_pb_pricing_table_wrap">
                            @foreach($package as $row)
                                <div class="et_pb_pricing_table et_pb_pricing_table_0 et_pb_featured_table">
                                    <div class="et_pb_pricing_heading">
                                        <h2 class="et_pb_pricing_title">{{$row->name}}</h2>
                                        <span class="et_pb_best_value">{{$row->description}}</span>
                                    </div> <!-- .et_pb_pricing_heading -->
                                    <div class="et_pb_pricing_content_top">
                                    <span class="et_pb_et_price"><span class="et_pb_dollar_sign">$</span><span
                                                class="et_pb_sum">{{$row->amount}}</span></span>
                                    </div> <!-- .et_pb_pricing_content_top -->
                                    <div class="et_pb_pricing_content">
                                        <ul class="et_pb_pricing">
                                            <li>
                                                @if(\Illuminate\Support\Facades\Auth::user())

                                                    <form method="POST" action="{{route('payment')}}">
                                                        @csrf
                                                        <input type="hidden" name="amount" value="{{$row->amount}}">
                                                        <input type="hidden" name="id" value="{{$row->id}}">
                                                        <input type="submit" value="Pay Now"
                                                               style="list-style: none; background: #FC1564; padding: 3%; color: #fff; border-raduis: 5px; text-align: center;"
                                                               class="wow-modal-id-1">
                                                    </form>

                                                @else
                                                    <a href="{{route('login')}}">
                                                        <span type="submit" value="Pay Now"
                                                              style="list-style: none; background: #FC1564; padding: 3%; color: #fff; border-raduis: 5px; text-align: center;"
                                                              class="wow-modal-id-1">Register
                                                            Now  </span>
                                                    </a>
                                                @endif()
                                            </li>
                                        </ul>
                                    </div> <!-- .et_pb_pricing_content -->

                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="et_pb_module et_pb_text et_pb_text_2 et_pb_bg_layout_light  et_pb_text_align_left">

                        <div class="row">

                            <div class="et_pb_text_inner"><h3 style="text-align:center; ">select a private CLASS
                                    PACKAGES&nbsp;</h3></div>
                        </div> <!-- .et_pb_text -->
                        <div class="et_pb_module et_pb_text et_pb_text_3 et_pb_bg_layout_light  et_pb_text_align_left">


                            <div class="et_pb_text_inner"><p style="text-align:center; padding:20px !important;">Virtual
                                    private training with Hot Booty Ballet trainers is now available for personally led
                                    workouts in the comfort of your own home. The HBB private sessions are delivered
                                    straight to your home, working one-on-one with you to customize a program that
                                    guarantees your success.</p></div>
                        </div> <!-- .et_pb_text -->
                    </div> <!-- .et_pb_column -->
                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">

                    </div>
                    <div class="et_pb_module et_pb_pricing_tables_1 et_pb_pricing clearfix et_pb_pricing_3 et_pb_pricing_no_bullet">


                        <div class="et_pb_pricing_table_wrap">
                            <div class="et_pb_pricing_table et_pb_pricing_table_3 et_pb_featured_table">


                                <div class="et_pb_pricing_heading">
                                    <h2 class="et_pb_pricing_title">Private Class A</h2>
                                    <span class="et_pb_best_value">Individual Class</span>
                                </div> <!-- .et_pb_pricing_heading -->
                                <div class="et_pb_pricing_content_top">
                                    <span class="et_pb_et_price"><span class="et_pb_dollar_sign">$</span><span
                                                class="et_pb_sum">125</span></span>
                                </div> <!-- .et_pb_pricing_content_top -->
                                <div class="et_pb_pricing_content">
                                    <ul class="et_pb_pricing">
                                        <li><span>
                                                <p style="text-align: center;">
                                                    <span
                                                            style="list-style: none; background: #FC1564; padding: 3%; color: #fff; border-raduis: 5px; text-align: center;"
                                                            class="wow-modal-id-1">Register Now</span></p>
                                            </span></li>
                                    </ul>
                                </div> <!-- .et_pb_pricing_content -->

                            </div>
                            <div class="et_pb_pricing_table et_pb_pricing_table_4 et_pb_featured_table">


                                <div class="et_pb_pricing_heading">
                                    <h2 class="et_pb_pricing_title">Private Class B</h2>
                                    <span class="et_pb_best_value">5 Classes</span>
                                </div> <!-- .et_pb_pricing_heading -->
                                <div class="et_pb_pricing_content_top">
                                    <span class="et_pb_et_price"><span class="et_pb_dollar_sign">$</span><span
                                                class="et_pb_sum">550</span></span>
                                </div> <!-- .et_pb_pricing_content_top -->
                                <div class="et_pb_pricing_content">
                                    <ul class="et_pb_pricing">
                                        <li><span><p style="text-align: center;"><span
                                                            style="list-style: none; background: #FC1564; padding: 3%; color: #fff; border-raduis: 5px; text-align: center;"
                                                            class="wow-modal-id-1">Register Now</span></p></span></li>
                                    </ul>
                                </div> <!-- .et_pb_pricing_content -->

                            </div>
                            <div class="et_pb_pricing_table et_pb_pricing_table_5 et_pb_featured_table">


                                <div class="et_pb_pricing_heading">
                                    <h2 class="et_pb_pricing_title">Private Class C</h2>
                                    <span class="et_pb_best_value">10 Classes</span>
                                </div> <!-- .et_pb_pricing_heading -->
                                <div class="et_pb_pricing_content_top">
                                    <span class="et_pb_et_price"><span class="et_pb_dollar_sign">$</span><span
                                                class="et_pb_sum">1000</span></span>
                                </div> <!-- .et_pb_pricing_content_top -->
                                <div class="et_pb_pricing_content">
                                    <ul class="et_pb_pricing">
                                        <li><span><p style="text-align: center;"><span
                                                            style="list-style: none; background: #FC1564; padding: 3%; color: #fff; border-raduis: 5px; text-align: center;"
                                                            class="wow-modal-id-1">Register Now</span></p></span></li>
                                    </ul>
                                </div> <!-- .et_pb_pricing_content -->

                            </div>
                        </div>
                    </div>
                </div> <!-- .et_pb_column -->


            </div> <!-- .et_pb_row -->


        </div> <!-- .et_pb_section -->
        <div class="et_pb_section et_pb_section_3 et_section_regular">
            <div class="container">
                <div class="row instruc">
                    <h1>OUR INSTRUCTORS</h1>
                    <div class="col-md-2"><img src="public/images/06-SUAAD.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-KATHERINE.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-LUDOVIC.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-PALOMA.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-SHELLY.jpg"/></div>

                </div>
            </div>


            <!-- .et_pb_column -->


        </div> <!-- .et_pb_row -->
        <div class="et_pb_row et_pb_row_5">
            <div class="et_pb_column et_pb_column_4_4 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough et-last-child">


                <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                    <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_hover_enabled et_pb_bg_layout_light"
                       href="online-studio.html" data-icon="&#57392;">check out the full studio!</a>
                </div>
            </div> <!-- .et_pb_column -->


        </div> <!-- .et_pb_row -->


</div> <!-- .et_pb_section -->            </div>

</div>                    </div> <!-- .entry-content -->


</article><!-- .et_pb_post --></div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
@include('templates/footer')