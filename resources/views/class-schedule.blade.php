@include('templates/header')
<style>

    section.et_pb_module.et_pb_fullwidth_header.et_pb_fullwidth_header_0.et_pb_bg_layout_dark.et_pb_text_align_center.et_pb_fullscreen {
        background-image: url(/public/images/02-23.jpeg) !important;
    }

    .et_pb_row_2 {
        width: 85% !important;
        max-width: 2560px;
    }

    .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #fc1564 !important;
        border-color: #fc1564 !important;
    }


    div.et_pb_section.et_pb_section_1 {
        background-image: url(https://www.hotbootyballet.com/wp-content/uploads/2020/06/website-Virtual-classes.jpg) !important;
    }

    .introduction-s {
        background-image: linear-gradient(180deg, #00000075 0%, #0000002b 100%), url(public/images/website-Virtual-classes.jpg);
        background-size: cover;
        padding-top: 20%;
        padding-bottom: 10%;
    }

    .pagination {
        clear: both;
        display: inline-flex;
    }

    .pagination > li > a {

        color: #ea196a !important;

    }

button.btn.btn-sm.btn-success {
    background: #ea196a !important;}
    .pagination > .active > a {
        background-color: #000000;
        border-color: #fc1564;
    }

    .introduction-s h1 {

        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        font-weight: 300;
        text-transform: uppercase;
        font-size: 80px;
        text-align: center;
        text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4);
        color: white;
    }

    .introduction-s span {
        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        text-transform: uppercase;
        font-size: 24px;
        line-height: 1.5em;
        text-align: center;
        color: white;
        text-shadow: 0em 0.1em 0.1em rgba(0, 0, 0, 0.4);
        padding-bottom: 10%;
    }

    .row.instruc img {
        height: 175px;
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

    .row.instruc .col-md-2 {
        width: 19.666667%;
    }

    .row.instruc h1 {
        font-family: 'Roboto Condensed', Helvetica, Arial, Lucida, sans-serif;
        font-variant: small-caps;
        font-size: 5vw;
        color: #fc1564 !important;
        letter-spacing: 0em;
        line-height: 1.2em;
        text-align: center;
    }

    body .et_pb_button {
        border-color: #fc156400;

    }

    .hm-gradient {
        background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
    }

    .darken-grey-text {
        color: #2E2E2E;
    }

    .input-group.md-form.form-sm.form-2 input {
        border: 1px solid #bdbdbd;
        border-top-left-radius: 0.25rem;
        border-bottom-left-radius: 0.25rem;
    }

    .input-group.md-form.form-sm.form-2 input.purple-border {
        border: 1px solid #9e9e9e;
    }

    .input-group.md-form.form-sm.form-2 input[type=text]:focus:not([readonly]).purple-border {
        border: 1px solid #ba68c8;
        box-shadow: none;
    }

    .form-2 .input-group-addon {
        border: 1px solid #ba68c8;
    }

    .danger-text {
        color: #ff3547;
    }

    .success-text {
        color: #00C851;
    }

    .table-bordered.red-border, .table-bordered.red-border th, .table-bordered.red-border td {
        border: 1px solid #ff3547 !important;
    }

    .table.table-bordered th {
        text-align: center;
    }
</style>

<div id="main-content">

    <div class="introduction-s">
        <h1 class="et_pb_module_header">ClASS Schedule</h1>
        <span class="et_pb_fullwidth_header_subhead"></span>
    </div>

    <article id="post-52454" class="post-52454 page type-page status-publish hentry">
        <div class="entry-content">


        </div> <!-- .et_pb_section -->
        <div class="et_pb_section et_pb_section_2 et_section_regular">


            <div class="et_pb_row et_pb_row_1">
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough et-last-child">


                    <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">


                        <div class="et_pb_text_inner"><h3 style="text-align:center;">select a VIRTUAL CLASS SCHEDULE&nbsp;</h3>
                        </div>
                    </div> <!-- .et_pb_text -->
                    <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">


                        <div class="et_pb_text_inner"><p style="text-align:center;">HOT BOOTY BALLET virtual classes are
                                presented using the Zoom app that is easily downloaded via your App Store or web
                                browser. To attend any of HBB classes, sign up via www.hotbootyballet.com website. You
                                can purchase individual classes or a 10 class bundle and choose any virtual classes on
                                the schedule.</p></div>
                    </div> <!-- .et_pb_text -->
                </div> <!-- .et_pb_column -->


            </div> <!-- .et_pb_row -->


            <div class="container mt-4">
                <div class="card">
                    <div class="card-body">
                        <!-- Grid row -->
                        <div class="row">
                            <!-- Grid column -->
                            <div class="col-md-12">
                            </div>
                            <!-- Grid column -->
                        </div>
                        <!-- Grid row -->
                        <!--Table-->
                        <table class="table table-hover table-responsive mb-0">
                            <!--Table head-->
                            <thead>
                            <tr>
                                <th class="th-lg"><a>Date</a></th>
                                <th class="th-lg"><a>DAY</a></th>
                                <th class="th-lg"><a>CLASS TYPE</a></th>
                                <th class="th-lg"><a>TIME</a></th>
                                <th class="th-lg"><a>EQUIPMENT</a></th>
                                <th class="th-lg"><a>INSTRUCTOR</a></th>
                                <th class="th-lg"><a>ACTION</a></th>


                            </tr>
                            </thead>
                            <!--Table head-->
                            <!--Table body-->
                            <tbody>

                            @foreach($schedule as $row)
                                <tr>
                                    <td>{{$row->date}}</td>
                                    <td>@if($row->day == 0) Monday
                                        @elseif($row->day == 1) Tuesday
                                        @elseif($row->day == 2) Wednesday
                                        @elseif($row->day == 3) Thursday
                                        @elseif($row->day == 4) Friday
                                        @elseif($row->day == 5) Saturday
                                        @else Sunday @endif</td>
                                    <td>{{$row->type}}</td>
                                    <td>{{$row->time}}</td>
                                    <td>{{$row->equipment}}</td>
                                    <td>{{$row->instructor}}</td>
                                    <td> @if(\Illuminate\Support\Facades\Auth::user())
                                            @if($row->booking->count() > 0)
                                                @foreach($row->booking as $book)
                                                    @if($book->user_id == \Illuminate\Support\Facades\Auth::id())
                                                        <button>Check Booking</button>
                                                    @else
                                                        <form method="POST" action="{{ route('subscribe') }}">
                                                            @csrf
                                                            <input name="user_id" type="hidden"
                                                                   value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                                            <input name="subscription_id" type="hidden"
                                                                   value="{{$row->id}}">
                                                            <input type="submit" value="Subscribe">
                                                        </form>
                                                    @endif()

                                                @endforeach
                                            @else
                                                <form method="POST" action="{{ route('subscribe') }}">
                                                    @csrf
                                                    <input name="user_id" type="hidden"
                                                           value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                                    <input name="schedule_id" type="hidden"
                                                           value="{{$row->id}}">
                                                    <input type="submit" value="Subscribe">
                                                </form>
                                            @endif()

                                        @else
                                            <a href="{{route('login')}}">
                                                <button>Login to Book</button>
                                            </a>
                                        @endif()</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <!--Table body-->
                        </table>
                        <!--Bottom Table UI-->
                    {{ $schedule->links() }}
                    <!--Bottom Table UI-->
                    </div>
                </div>

            </div>
        </div>

        <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
            <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_hover_enabled et_pb_bg_layout_light"
               href="online-studio.html" data-icon="&#57392;">Sign Up For Virtual Class Now</a>
        </div>
        <!-- .et_pb_section -->
        <div class="et_pb_section et_pb_section_3 et_section_regular">
            <div class="container">
                <div class="row instruc">
                    <h1>OUR INSTRUCTORS</h1>
                    <div class="col-md-2"><img src="public/images/06-SUAAD.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-KATHERINE.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-LUDOVIC.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-PALOMA.jpg"/></div>
                    <div class="col-md-2"><img src="public/images/06-SHELLY.jpg"/></div>

                </div>
            </div>


            <!-- .et_pb_column -->


        </div> <!-- .et_pb_row -->
        <div class="et_pb_row et_pb_row_5">
            <div class="et_pb_column et_pb_column_4_4 et_pb_column_10  et_pb_css_mix_blend_mode_passthrough et-last-child">


                <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                    <a class="et_pb_button et_pb_custom_button_icon et_pb_button_0 et_hover_enabled et_pb_bg_layout_light"
                       href="online-studio.html" data-icon="&#57392;">check out the full studio!</a>
                </div>
            </div> <!-- .et_pb_column -->


        </div> <!-- .et_pb_row -->


</div> <!-- .et_pb_section -->            </div>

</div>                    </div> <!-- .entry-content -->


</article><!-- .et_pb_post --></div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
@include('templates/footer')