@include('templates/header')

<div class="main-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-8">
                <h2>Phone numbers</h2>
                <div class="rTable">
                    <div class="rTableRow">
                        <div class="rTableHead"><strong>Date</strong></div>
                        <div class="rTableHead"><strong>Day</strong></div>
                        <div class="rTableHead"><strong>Class Type</strong></div>
                        <div class="rTableHead"><strong>Time</strong></div>
                        <div class="rTableHead"><strong>Equipment</strong></div>
                        <div class="rTableHead"><strong>Instructor</strong></div>
                        <div class="rTableHead"><strong>Info</strong></div>
                    </div>
                    @foreach($schedule as $row)
                        <div class="rTableRow">
                            <div class="rTableCell">{{$row->date}}</div>
                            <div class="rTableCell">@if($row->day == 0) Monday
                                @elseif($row->day == 1) Tuesday
                                @elseif($row->day == 2) Wednesday
                                @elseif($row->day == 3) Thursday
                                @elseif($row->day == 4) Friday
                                @elseif($row->day == 5) Saturday
                                @else Sunday @endif </div>
                            <div class="rTableCell">{{$row->type}}</div>
                            <div class="rTableCell">{{$row->time}}</div>
                            <div class="rTableCell">{{$row->equipment}}</div>
                            <div class="rTableCell">{{$row->instructor}}</div>
                            <div class="rTableCell">
                                @if(\Illuminate\Support\Facades\Auth::user())
                                    @if($row->booking->count() > 0)
                                        @foreach($row->booking as $book)
                                            @if($book->user_id == \Illuminate\Support\Facades\Auth::id())
                                                <button>Check Booking</button>
                                            @else
                                                <form method="POST" action="{{ route('subscribe') }}">
                                                    @csrf
                                                    <input name="user_id" type="hidden"
                                                           value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                                    <input name="subscription_id" type="hidden"
                                                           value="{{$row->id}}">
                                                    <input type="submit" value="Subscribe">
                                                </form>
                                            @endif()

                                        @endforeach
                                    @else
                                        <form method="POST" action="{{ route('subscribe') }}">
                                            @csrf
                                            <input name="user_id" type="hidden"
                                                   value="{{\Illuminate\Support\Facades\Auth::id()}}">
                                            <input name="schedule_id" type="hidden"
                                                   value="{{$row->id}}">
                                            <input type="submit" value="Subscribe">
                                        </form>
                                    @endif()

                                @else
                                    <a href="{{route('login')}}">
                                        <button>Login to Book</button>
                                    </a>
                                @endif()

                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .rTable {
        display: table;
    }

    .rTableRow {
        display: table-row;
    }

    .rTableHeading {
        display: table-header-group;
    }

    .rTableBody {
        display: table-row-group;
    }

    .rTableFoot {
        display: table-footer-group;
    }

    .rTableCell, .rTableHead {
        display: table-cell;
    }

    .rTable {
        display: table;
        width: 100%;
    }

    .rTableRow {
        display: table-row;
    }

    .rTableHeading {
        display: table-header-group;
        background-color: #ddd;
    }

    .rTableCell, .rTableHead {
        display: table-cell;
        padding: 3px 10px;
        border: 1px solid #999999;
    }

    .rTableHeading {
        display: table-header-group;
        background-color: #ddd;
        font-weight: bold;
    }

    .rTableFoot {
        display: table-footer-group;
        font-weight: bold;
        background-color: #ddd;
    }

    .rTableBody {
        display: table-row-group;
    }
</style>
@include('templates/footer')