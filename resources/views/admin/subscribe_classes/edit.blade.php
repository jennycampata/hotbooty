@extends('admin.layouts.app')

@section('title')
    {{ $subscribeClass->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($subscribeClass, ['route' => ['admin.subscribe-classes.update', $subscribeClass->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.subscribe_classes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection