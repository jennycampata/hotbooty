@extends('admin.layouts.app')

@section('title')
    {{ $booking->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($booking, ['route' => ['admin.bookings.update', $booking->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.bookings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection