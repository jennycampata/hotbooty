<!-- Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day', 'Day:') !!}
    {!! Form::select('day', $days, null, ['class' => 'form-control select2']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control', 'placeholder'=>'Enter type']) !!}
</div>

<!-- Time Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time', 'Time:') !!}
    {!! Form::text('time', null, ['class' => 'form-control', 'placeholder'=>'Enter time']) !!}
</div>

<!-- Equipment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment', 'Equipment:') !!}
    {!! Form::text('equipment', null, ['class' => 'form-control', 'placeholder'=>'Enter equipment']) !!}
</div>

<!-- Instructor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instructor', 'Instructor:') !!}
    {!! Form::text('instructor', null, ['class' => 'form-control', 'placeholder'=>'Enter instructor']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($schedule))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.schedules.index') !!}" class="btn btn-default">Cancel</a>
</div>