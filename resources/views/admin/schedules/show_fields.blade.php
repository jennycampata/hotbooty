<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $schedule->id !!}</dd>

<!-- Day Field -->
<dt>{!! Form::label('day', 'Day:') !!}</dt>
<dd>{!! $schedule->day !!}</dd>

<!-- Type Field -->
<dt>{!! Form::label('type', 'Type:') !!}</dt>
<dd>{!! $schedule->type !!}</dd>

<!-- Time Field -->
<dt>{!! Form::label('time', 'Time:') !!}</dt>
<dd>{!! $schedule->time !!}</dd>

<!-- Equipment Field -->
<dt>{!! Form::label('equipment', 'Equipment:') !!}</dt>
<dd>{!! $schedule->equipment !!}</dd>

<!-- Instructor Field -->
<dt>{!! Form::label('instructor', 'Instructor:') !!}</dt>
<dd>{!! $schedule->instructor !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $schedule->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $schedule->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $schedule->deleted_at !!}</dd>

