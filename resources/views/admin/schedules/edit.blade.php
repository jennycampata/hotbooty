@extends('admin.layouts.app')

@section('title')
    {{ $schedule->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($schedule, ['route' => ['admin.schedules.update', $schedule->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.schedules.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection