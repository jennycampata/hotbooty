@extends('admin.layouts.app')

@section('title')
    {{ $day->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($day, ['route' => ['admin.days.update', $day->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.days.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection