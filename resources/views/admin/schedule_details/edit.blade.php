@extends('admin.layouts.app')

@section('title')
    {{ $scheduleDetail->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($scheduleDetail, ['route' => ['admin.schedule-details.update', $scheduleDetail->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.schedule_details.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection