<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $scheduleDetail->id !!}</dd>

<!-- Schedule Id Field -->
<dt>{!! Form::label('schedule_id', 'Schedule Id:') !!}</dt>
<dd>{!! $scheduleDetail->schedule_id !!}</dd>

<!-- Description Field -->
<dt>{!! Form::label('description', 'Description:') !!}</dt>
<dd>{!! $scheduleDetail->description !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $scheduleDetail->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $scheduleDetail->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $scheduleDetail->deleted_at !!}</dd>

