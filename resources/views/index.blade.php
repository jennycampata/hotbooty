<!DOCTYPE html>
<html>
<head>
<title>Home - Hot Booty Ballet</title><!-- This site is optimized with the Yoast SEO plugin v11.9 - https://yoast.com/wordpress/plugins/seo/ --><link rel="canonical" href="https://www.hotbootyballet.com/"><meta property="og:locale" content="en_US"><meta property="og:type" content="website"><meta property="og:title" content="Home - Hot Booty Ballet"><meta property="og:url" content="https://www.hotbootyballet.com/"><meta property="og:site_name" content="Hot Booty Ballet"><meta name="twitter:card" content="summary_large_image"><meta name="twitter:title" content="Home - Hot Booty Ballet"><meta name="twitter:site" content="@suaadfitness"><meta name="twitter:creator" content="@suaadfitness">
<link rel="dns-prefetch" href="//fonts.googleapis.com"><link rel="dns-prefetch" href="//s.w.org"><link rel="alternate" type="application/rss+xml" title="Hot Booty Ballet &raquo; Feed" href="https://www.hotbootyballet.com/feed/"><link rel="alternate" type="application/rss+xml" title="Hot Booty Ballet &raquo; Comments Feed" href="https://www.hotbootyballet.com/comments/feed/"><meta content="Divi Child v.1.0.0" name="generator"><style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style><link rel="stylesheet" id="mp-theme-css" href="css/ui-theme.css" type="text/css" media="all"><link rel="stylesheet" id="mp-plans-css-css" href="css/css-plans.min.css" type="text/css" media="all"><link rel="stylesheet" id="wp-block-library-css" href="css/block-library-style.min.css" type="text/css" media="all"><link rel="stylesheet" id="wc-block-style-css" href="css/build-style.css" type="text/css" media="all"><link rel="stylesheet" id="contact-form-7-css" href="css/css-styles.css" type="text/css" media="all"><link rel="stylesheet" id="normalize_css-css" href="css/css-normalize.css" type="text/css" media="all"><link rel="stylesheet" id="custom_style_css-css" href="css/css-style.css" type="text/css" media="all"><link rel="stylesheet" id="woocommerce-layout-css" href="css/css-woocommerce-layout.css" type="text/css" media="all"><style id="woocommerce-layout-inline-css" type="text/css">

	.infinite-scroll .woocommerce-pagination {
		display: none;
	}
</style><link rel="stylesheet" id="woocommerce-smallscreen-css" href="css/css-woocommerce-smallscreen.css" type="text/css" media="only screen and (max-width: 768px)"><link rel="stylesheet" id="woocommerce-general-css" href="css/css-woocommerce.css" type="text/css" media="all"><style id="woocommerce-inline-inline-css" type="text/css">
.woocommerce form .form-row .required { visibility: visible; }
</style><link rel="stylesheet" id="parent-style-css" href="css/Divi-style.css" type="text/css" media="all"><link rel="stylesheet" id="divi-fonts-css" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,latin-ext" type="text/css" media="all"><link rel="stylesheet" id="divi-style-css" href="css/divi-child-style.css" type="text/css" media="all"><link rel="stylesheet" id="et-builder-googlefonts-cached-css" href="https://fonts.googleapis.com/css?family=Oswald%3A200%2C300%2Cregular%2C500%2C600%2C700%7CRoboto+Condensed%3A300%2C300italic%2Cregular%2Citalic%2C700%2C700italic&amp;ver=5.2.4#038;subset=latin,latin-ext" type="text/css" media="all"><link rel="stylesheet" id="popup-maker-site-css" href="css/pum-pum-site-styles.css" type="text/css" media="all"><link rel="stylesheet" id="dashicons-css" href="css/css-dashicons.min.css" type="text/css" media="all"><link rel="stylesheet" id="video-conferencing-with-zoom-api-css" href="css/css-main.min.css" type="text/css" media="all"><script type="text/javascript" src="js/jquery-jquery.js"></script><script type="text/javascript" src="js/jquery-jquery-migrate.min.js"></script><script type="text/javascript" src="js/js-snap.svg-min.js"></script><script type="text/javascript" src="js/js-modernizr.custom.js"></script><link rel="https://api.w.org/" href="https://www.hotbootyballet.com/wp-json/"><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.hotbootyballet.com/xmlrpc.php?rsd"><link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.hotbootyballet.com/wp-includes/wlwmanifest.xml"><meta name="generator" content="WordPress 5.2.4"><meta name="generator" content="WooCommerce 4.0.1"><link rel="shortlink" href="https://www.hotbootyballet.com/"><link rel="alternate" type="application/json+oembed" href="https://www.hotbootyballet.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.hotbootyballet.com%2F"><link rel="alternate" type="text/xml+oembed" href="https://www.hotbootyballet.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.hotbootyballet.com%2F&amp;format=xml"><style type="text/css">
		a {
cursor: pointer !important;
  
}
	</style><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"><link rel="shortcut icon" href="https://www.hotbootyballet.com/wp-content/uploads/2018/03/Hot_Booty_Ballet_logo.jpg"><meta name="google-site-verification" content="NJhQh3vjh4EDpZdwnOJdkmBSev-z9jPoRqEQGaiaxig"><meta name="google-site-verification" content="b0X4SOp3SOgUi_c1j9l5tUbFhwrxDprdyMP1SZXSd1o"><!-- Global site tag (gtag.js) - Google Analytics --><link rel="stylesheet" id="et-core-unified-cached-inline-styles" href="css/47810-et-core-unified-159131536863.min.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)"></head><body data-rsssl="1" class="home page-template-default page page-id-47810 theme-Divi woocommerce-no-js et_pb_button_helper_class et_fullwidth_nav et_fullwidth_secondary_nav et_fixed_nav et_show_nav et_cover_background et_secondary_nav_enabled et_secondary_nav_two_panels et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_slide et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_pb_pagebuilder_layout et_smooth_scroll et_right_sidebar et_divi_theme et-db et_minified_js et_minified_css">
	<div id="page-container">

					<div id="top-header">
			<div class="container clearfix">

			
				<div id="et-info">
									<span id="et-info-phone">514-264-9898</span>
				
									<a href="mailto:info@hotbootyballet.com"><span id="et-info-email">info@hotbootyballet.com</span></a>
				
				<ul class="et-social-icons"><li class="et-social-icon et-social-facebook">
		<a href="https://www.facebook.com/suaadfitness" class="icon">
			<span>Facebook</span>
		</a>
	</li>
	<li class="et-social-icon et-social-twitter">
		<a href="https://twitter.com/suaadfitness?lang=en" class="icon">
			<span>Twitter</span>
		</a>
	</li>

</ul></div> <!-- #et-info -->

			
				<div id="et-secondary-menu">
				<div class="et_duplicate_social_icons">
								<ul class="et-social-icons"><li class="et-social-icon et-social-facebook">
		<a href="https://www.facebook.com/suaadfitness" class="icon">
			<span>Facebook</span>
		</a>
	</li>
	<li class="et-social-icon et-social-twitter">
		<a href="https://twitter.com/suaadfitness?lang=en" class="icon">
			<span>Twitter</span>
		</a>
	</li>

</ul></div><a href="www.hotbootyballet.html" class="et-cart-info">
				<span>0 Items</span>
			</a>				</div> <!-- #et-secondary-menu -->

			</div> <!-- .container -->
		</div> <!-- #top-header -->
		
	
			<header id="main-header" data-height-onload="54"><div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="www.hotbootyballet.html">
						<img src="images/08-HBB-logo-black-4.png" alt="Hot Booty Ballet" id="logo" data-height-percentage="100"></a>
				</div>
							<div id="et-top-navigation" data-height="54" data-fixed-height="40">
											<nav id="top-menu-nav"><ul id="top-menu" class="nav"><li id="menu-item-48361" class="catalogue_button menu-item menu-item-type-post_type menu-item-object-page menu-item-48361"><a href="online-studio.html">Online Studio</a></li>
<li id="menu-item-49455" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49455"><a href="become-a-member.html">Become a Member</a></li>
<li id="menu-item-50052" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50052"><a href="become-an-instructor.html">Become an instructor</a></li>
<li id="menu-item-48764" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48764"><a href="our-story.html">Our Story</a></li>
<li id="menu-item-50176" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50176"><a href="visit-us.html">Find a Class</a></li>
<li id="menu-item-48356" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-48356"><a href="login.html">Log In</a></li>
</ul></nav><div id="et_mobile_nav_menu">
				<div class="mobile_nav closed">
					<span class="select_page">Select Page</span>
					<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
				</div>
			</div>				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="https://www.hotbootyballet.com/">
					<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:"></form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header><!-- #main-header --><div id="et-main-area">
	
<div id="main-content">



<!-- #CONNTENT GOES FROM HERE-->
			

	<span class="et_pb_scroll_top et-pb-icon"></span>


			<footer id="main-footer"><div id="footer-bottom">
					<div class="container clearfix">
				<ul class="et-social-icons"><li class="et-social-icon et-social-facebook">
		<a href="https://www.facebook.com/suaadfitness" class="icon">
			<span>Facebook</span>
		</a>
	</li>
	<li class="et-social-icon et-social-twitter">
		<a href="https://twitter.com/suaadfitness?lang=en" class="icon">
			<span>Twitter</span>
		</a>
	</li>

</ul><div id="footer-info">&copy; Hot Booty Ballet. 2019 All Rights Reserved.</div>					</div>	<!-- .container -->
				</div>
			</footer><!-- #main-footer -->
			
			
			
			
			
			</div> <!-- #et-main-area -->


	</div> <!-- #page-container -->

	

	<script type="text/javascript" src="js/frontend-cart-fragments.min.js"></script><script type="text/javascript">
		jQuery( 'body' ).bind( 'wc_fragments_refreshed', function() {
			jQuery( 'body' ).trigger( 'jetpack-lazy-images-load' );
		} );
	
</script><script type="text/javascript" src="js/js-custom.min.js"></script><script type="text/javascript" src="js/ui-core.min.js"></script><script type="text/javascript" src="js/ui-position.min.js"></script>
<script type="text/javascript" src="js/pum-pum-site-scripts.js"></script><script type="text/javascript" src="js/js-common.js"></script><script type="text/javascript" src="js/js-wp-embed.min.js"></script><script type="text/javascript" src="js/js-jquery.exitintent.js"></script><script type="text/javascript" src="js/js-custom.js"></script></body></html>
