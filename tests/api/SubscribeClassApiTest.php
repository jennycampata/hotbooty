<?php

namespace Tests\Api;

use Tests\ApiTestTrait;
use Tests\TestCase;
use Tests\Traits\MakeSubscribeClassTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscribeClassApiTest extends TestCase
{
    use MakeSubscribeClassTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSubscribeClass()
    {
        $subscribeClass = $this->fakeSubscribeClassData();
        $this->json('POST', '/api/v1/subscribe-classes', $subscribeClass);

        $this->assertApiResponse($subscribeClass);
    }

    /**
     * @test
     */
    public function testReadSubscribeClass()
    {
        $subscribeClass = $this->makeSubscribeClass();
        $this->json('GET', '/api/v1/subscribe-classes/'.$subscribeClass->id);

        $this->assertApiResponse($subscribeClass->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSubscribeClass()
    {
        $subscribeClass = $this->makeSubscribeClass();
        $editedSubscribeClass = $this->fakeSubscribeClassData();

        $this->json('PUT', '/api/v1/subscribe-classes/'.$subscribeClass->id, $editedSubscribeClass);

        $this->assertApiResponse($editedSubscribeClass);
    }

    /**
     * @test
     */
    public function testDeleteSubscribeClass()
    {
        $subscribeClass = $this->makeSubscribeClass();
        $this->json('DELETE', '/api/v1/subscribe-classes/'.$subscribeClass->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/subscribe-classes/'.$subscribeClass->id);

        $this->assertResponseStatus(404);
    }
}
