<?php

namespace Tests\Api;

use Tests\ApiTestTrait;
use Tests\TestCase;
use Tests\Traits\MakeScheduleDetailTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ScheduleDetailApiTest extends TestCase
{
    use MakeScheduleDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateScheduleDetail()
    {
        $scheduleDetail = $this->fakeScheduleDetailData();
        $this->json('POST', '/api/v1/schedule-details', $scheduleDetail);

        $this->assertApiResponse($scheduleDetail);
    }

    /**
     * @test
     */
    public function testReadScheduleDetail()
    {
        $scheduleDetail = $this->makeScheduleDetail();
        $this->json('GET', '/api/v1/schedule-details/'.$scheduleDetail->id);

        $this->assertApiResponse($scheduleDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateScheduleDetail()
    {
        $scheduleDetail = $this->makeScheduleDetail();
        $editedScheduleDetail = $this->fakeScheduleDetailData();

        $this->json('PUT', '/api/v1/schedule-details/'.$scheduleDetail->id, $editedScheduleDetail);

        $this->assertApiResponse($editedScheduleDetail);
    }

    /**
     * @test
     */
    public function testDeleteScheduleDetail()
    {
        $scheduleDetail = $this->makeScheduleDetail();
        $this->json('DELETE', '/api/v1/schedule-details/'.$scheduleDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/schedule-details/'.$scheduleDetail->id);

        $this->assertResponseStatus(404);
    }
}
