<?php

namespace Tests\Repository;

use \App;
use Tests\ApiTestTrait;
use Tests\TestCase;
use \Tests\Traits\MakeSubscribeClassTrait;
use App\Models\SubscribeClass;
use App\Repositories\Admin\SubscribeClassRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubscribeClassRepositoryTest extends TestCase
{
    use MakeSubscribeClassTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SubscribeClassRepository
     */
    protected $subscribeClassRepo;

    public function setUp()
    {
        parent::setUp();
        $this->subscribeClassRepo = App::make(SubscribeClassRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSubscribeClass()
    {
        $subscribeClass = $this->fakeSubscribeClassData();
        $createdSubscribeClass = $this->subscribeClassRepo->create($subscribeClass);
        $createdSubscribeClass = $createdSubscribeClass->toArray();
        $this->assertArrayHasKey('id', $createdSubscribeClass);
        $this->assertNotNull($createdSubscribeClass['id'], 'Created SubscribeClass must have id specified');
        $this->assertNotNull(SubscribeClass::find($createdSubscribeClass['id']), 'SubscribeClass with given id must be in DB');
        $this->assertModelData($subscribeClass, $createdSubscribeClass);
    }

    /**
     * @test read
     */
    public function testReadSubscribeClass()
    {
        $subscribeClass = $this->makeSubscribeClass();
        $dbSubscribeClass = $this->subscribeClassRepo->find($subscribeClass->id);
        $dbSubscribeClass = $dbSubscribeClass->toArray();
        $this->assertModelData($subscribeClass->toArray(), $dbSubscribeClass);
    }

    /**
     * @test update
     */
    public function testUpdateSubscribeClass()
    {
        $subscribeClass = $this->makeSubscribeClass();
        $fakeSubscribeClass = $this->fakeSubscribeClassData();
        $updatedSubscribeClass = $this->subscribeClassRepo->update($fakeSubscribeClass, $subscribeClass->id);
        $this->assertModelData($fakeSubscribeClass, $updatedSubscribeClass->toArray());
        $dbSubscribeClass = $this->subscribeClassRepo->find($subscribeClass->id);
        $this->assertModelData($fakeSubscribeClass, $dbSubscribeClass->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSubscribeClass()
    {
        $subscribeClass = $this->makeSubscribeClass();
        $resp = $this->subscribeClassRepo->delete($subscribeClass->id);
        $this->assertTrue($resp);
        $this->assertNull(SubscribeClass::find($subscribeClass->id), 'SubscribeClass should not exist in DB');
    }
}
