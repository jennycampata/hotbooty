<?php

namespace Tests\Repository;

use \App;
use Tests\ApiTestTrait;
use Tests\TestCase;
use \Tests\Traits\MakeScheduleDetailTrait;
use App\Models\ScheduleDetail;
use App\Repositories\Admin\ScheduleDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ScheduleDetailRepositoryTest extends TestCase
{
    use MakeScheduleDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ScheduleDetailRepository
     */
    protected $scheduleDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->scheduleDetailRepo = App::make(ScheduleDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateScheduleDetail()
    {
        $scheduleDetail = $this->fakeScheduleDetailData();
        $createdScheduleDetail = $this->scheduleDetailRepo->create($scheduleDetail);
        $createdScheduleDetail = $createdScheduleDetail->toArray();
        $this->assertArrayHasKey('id', $createdScheduleDetail);
        $this->assertNotNull($createdScheduleDetail['id'], 'Created ScheduleDetail must have id specified');
        $this->assertNotNull(ScheduleDetail::find($createdScheduleDetail['id']), 'ScheduleDetail with given id must be in DB');
        $this->assertModelData($scheduleDetail, $createdScheduleDetail);
    }

    /**
     * @test read
     */
    public function testReadScheduleDetail()
    {
        $scheduleDetail = $this->makeScheduleDetail();
        $dbScheduleDetail = $this->scheduleDetailRepo->find($scheduleDetail->id);
        $dbScheduleDetail = $dbScheduleDetail->toArray();
        $this->assertModelData($scheduleDetail->toArray(), $dbScheduleDetail);
    }

    /**
     * @test update
     */
    public function testUpdateScheduleDetail()
    {
        $scheduleDetail = $this->makeScheduleDetail();
        $fakeScheduleDetail = $this->fakeScheduleDetailData();
        $updatedScheduleDetail = $this->scheduleDetailRepo->update($fakeScheduleDetail, $scheduleDetail->id);
        $this->assertModelData($fakeScheduleDetail, $updatedScheduleDetail->toArray());
        $dbScheduleDetail = $this->scheduleDetailRepo->find($scheduleDetail->id);
        $this->assertModelData($fakeScheduleDetail, $dbScheduleDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteScheduleDetail()
    {
        $scheduleDetail = $this->makeScheduleDetail();
        $resp = $this->scheduleDetailRepo->delete($scheduleDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(ScheduleDetail::find($scheduleDetail->id), 'ScheduleDetail should not exist in DB');
    }
}
