<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\Day;
use App\Models\Schedule;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class ScheduleDataTable
 * @package App\DataTables\Admin
 */
class ScheduleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $dataTable->editColumn('date', function ($date) {
            return date('M-d-Y', strtotime($date->date));
        });
        $dataTable->editColumn('day', function ($day) {
            if ($day->day == 0) {
                return 'Monday';
            } else if ($day->day == 1) {
                return 'Tuesday';
            } else if ($day->day == 2) {
                return 'Wednesday';
            } else if ($day->day == 3) {
                return 'Thursday';
            } else if ($day->day == 4) {
                return 'Friday';
            } else if ($day->day == 5) {
                return 'Saturday';
            } else {
                return 'Sunday';
            }
        });

        return $dataTable->addColumn('action', 'admin.schedules.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Schedule $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Schedule $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('schedules.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'date',
            'day',
            'type',
            'time',
            'equipment',
            'instructor'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'schedulesdatatable_' . time();
    }
}