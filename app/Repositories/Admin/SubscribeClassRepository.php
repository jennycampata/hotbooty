<?php

namespace App\Repositories\Admin;

use App\Models\SubscribeClass;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubscribeClassRepository
 * @package App\Repositories\Admin
 * @version June 5, 2020, 8:37 pm UTC
 *
 * @method SubscribeClass findWithoutFail($id, $columns = ['*'])
 * @method SubscribeClass find($id, $columns = ['*'])
 * @method SubscribeClass first($columns = ['*'])
 */
class SubscribeClassRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'class_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubscribeClass::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
//        $input = $request->all();
        $subscribeClass = $this->create($request);
        return $subscribeClass;
    }

    /**
     * @param $request
     * @param $subscribeClass
     * @return mixed
     */
    public function updateRecord($request, $subscribeClass)
    {
        $input = $request->all();
        $subscribeClass = $this->update($input, $subscribeClass->id);
        return $subscribeClass;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $subscribeClass = $this->delete($id);
        return $subscribeClass;
    }
}
