<?php

namespace App\Repositories\Admin;

use App\Models\Schedule;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ScheduleRepository
 * @package App\Repositories\Admin
 * @version June 5, 2020, 3:45 pm UTC
 *
 * @method Schedule findWithoutFail($id, $columns = ['*'])
 * @method Schedule find($id, $columns = ['*'])
 * @method Schedule first($columns = ['*'])
*/
class ScheduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'day',
        'type',
        'time',
        'equipment',
        'instructor'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Schedule::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $schedule = $this->create($input);
        return $schedule;
    }

    /**
     * @param $request
     * @param $schedule
     * @return mixed
     */
    public function updateRecord($request, $schedule)
    {
        $input = $request->all();
        $schedule = $this->update($input, $schedule->id);
        return $schedule;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $schedule = $this->delete($id);
        return $schedule;
    }
}
