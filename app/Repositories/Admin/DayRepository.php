<?php

namespace App\Repositories\Admin;

use App\Models\Day;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DayRepository
 * @package App\Repositories\Admin
 * @version June 5, 2020, 5:42 pm UTC
 *
 * @method Day findWithoutFail($id, $columns = ['*'])
 * @method Day find($id, $columns = ['*'])
 * @method Day first($columns = ['*'])
*/
class DayRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Day::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $day = $this->create($input);
        return $day;
    }

    /**
     * @param $request
     * @param $day
     * @return mixed
     */
    public function updateRecord($request, $day)
    {
        $input = $request->all();
        $day = $this->update($input, $day->id);
        return $day;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $day = $this->delete($id);
        return $day;
    }
}
