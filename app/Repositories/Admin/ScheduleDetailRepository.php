<?php

namespace App\Repositories\Admin;

use App\Models\ScheduleDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ScheduleDetailRepository
 * @package App\Repositories\Admin
 * @version June 5, 2020, 8:33 pm UTC
 *
 * @method ScheduleDetail findWithoutFail($id, $columns = ['*'])
 * @method ScheduleDetail find($id, $columns = ['*'])
 * @method ScheduleDetail first($columns = ['*'])
*/
class ScheduleDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'schedule_id',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ScheduleDetail::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $scheduleDetail = $this->create($input);
        return $scheduleDetail;
    }

    /**
     * @param $request
     * @param $scheduleDetail
     * @return mixed
     */
    public function updateRecord($request, $scheduleDetail)
    {
        $input = $request->all();
        $scheduleDetail = $this->update($input, $scheduleDetail->id);
        return $scheduleDetail;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $scheduleDetail = $this->delete($id);
        return $scheduleDetail;
    }
}
