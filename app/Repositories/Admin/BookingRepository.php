<?php

namespace App\Repositories\Admin;

use App\Models\Booking;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BookingRepository
 * @package App\Repositories\Admin
 * @version June 5, 2020, 10:04 pm UTC
 *
 * @method Booking findWithoutFail($id, $columns = ['*'])
 * @method Booking find($id, $columns = ['*'])
 * @method Booking first($columns = ['*'])
 */
class BookingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'schedule_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Booking::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $booking = $this->create($input);
        return $booking;
    }

    /**
     * @param $request
     * @param $booking
     * @return mixed
     */
    public function updateRecord($request, $booking)
    {
        $input = $request->all();
        $booking = $this->update($input, $booking->id);
        return $booking;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $booking = $this->delete($id);
        return $booking;
    }
}
