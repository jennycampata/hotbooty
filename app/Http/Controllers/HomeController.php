<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\UserClasses;
use App\Repositories\Admin\BookingRepository;
use App\Repositories\Admin\PackageRepository;
use App\Repositories\Admin\ScheduleRepository;
use App\Repositories\Admin\SubscribeClassRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;
use function foo\func;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $scheduleRepository;
    private $bookingRepository;
    private $subscribeClasses;
    private $packageRepository;
    private $session;

    public function __construct(ScheduleRepository $scheduleRepo, BookingRepository $bookingRepo, SubscribeClassRepository $subscribeRepo, PackageRepository $packageRepo, Store $session)
    {
        $this->scheduleRepository = $scheduleRepo;
        $this->bookingRepository = $bookingRepo;
        $this->subscribeClasses = $subscribeRepo;
        $this->packageRepository = $packageRepo;
        $this->session = $session;

//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    public function schedule()
    {

        $date = Carbon::today()->toDateString();
        $user = Auth::id();
        $schedule = $this->scheduleRepository->where('date', '>=', $date)->orderBy('date', 'asc')->paginate(7);

        return view('class-schedule')->with([
            'schedule' => $schedule
        ]);
    }

    public function booking(Request $request)
    {
        $userClass = UserClasses::where('user_id', $request->user_id)->get();
        if ($userClass->count() > 0) {
            if ($userClass[0]->total > 0) {
                $total['total'] = $userClass[0]->total - 1;
                UserClasses::where('id', $userClass[0]->id)->update($total);
                $this->bookingRepository->saveRecord($request);
                return redirect(route('schedule'));
            } else {
                return redirect(route('classes'));
            }
        } else {
            return redirect(route('classes'));
        }


    }

    public function classes()
    {
        $package = $this->packageRepository->all();
        return view('virtual-class')->with([
            'package' => $package
        ]);
    }

    public function logout()
    {
        auth()->logout();
        $this->session->forget('remember_token');
        return redirect(route('home'));   }
}
