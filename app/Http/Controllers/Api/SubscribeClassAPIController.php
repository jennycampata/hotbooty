<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateSubscribeClassAPIRequest;
use App\Http\Requests\Api\UpdateSubscribeClassAPIRequest;
use App\Models\SubscribeClass;
use App\Repositories\Admin\SubscribeClassRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class SubscribeClassController
 * @package App\Http\Controllers\Api
 */

class SubscribeClassAPIController extends AppBaseController
{
    /** @var  SubscribeClassRepository */
    private $subscribeClassRepository;

    public function __construct(SubscribeClassRepository $subscribeClassRepo)
    {
        $this->subscribeClassRepository = $subscribeClassRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/subscribe-classes",
     *      summary="Get a listing of the SubscribeClasses.",
     *      tags={"SubscribeClass"},
     *      description="Get all SubscribeClasses",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/SubscribeClass")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $subscribeClasses = $this->subscribeClassRepository
            ->pushCriteria(new RequestCriteria($request))
            ->pushCriteria(new LimitOffsetCriteria($request))
            //->pushCriteria(new subscribeClassCriteria($request))
            ->all();

        return $this->sendResponse($subscribeClasses->toArray(), 'Subscribe Classes retrieved successfully');
    }

    /**
     * @param CreateSubscribeClassAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/subscribe-classes",
     *      summary="Store a newly created SubscribeClass in storage",
     *      tags={"SubscribeClass"},
     *      description="Store SubscribeClass",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SubscribeClass that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SubscribeClass")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SubscribeClass"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSubscribeClassAPIRequest $request)
    {
        $subscribeClasses = $this->subscribeClassRepository->saveRecord($request);

        return $this->sendResponse($subscribeClasses->toArray(), 'Subscribe Class saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/subscribe-classes/{id}",
     *      summary="Display the specified SubscribeClass",
     *      tags={"SubscribeClass"},
     *      description="Get SubscribeClass",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SubscribeClass",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SubscribeClass"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var SubscribeClass $subscribeClass */
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            return $this->sendErrorWithData(['Subscribe Class not found']);
        }

        return $this->sendResponse($subscribeClass->toArray(), 'Subscribe Class retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSubscribeClassAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/subscribe-classes/{id}",
     *      summary="Update the specified SubscribeClass in storage",
     *      tags={"SubscribeClass"},
     *      description="Update SubscribeClass",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SubscribeClass",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="SubscribeClass that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/SubscribeClass")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/SubscribeClass"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSubscribeClassAPIRequest $request)
    {
        /** @var SubscribeClass $subscribeClass */
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            return $this->sendErrorWithData(['Subscribe Class not found']);
        }

        $subscribeClass = $this->subscribeClassRepository->updateRecord($request, $subscribeClass);

        return $this->sendResponse($subscribeClass->toArray(), 'SubscribeClass updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/subscribe-classes/{id}",
     *      summary="Remove the specified SubscribeClass from storage",
     *      tags={"SubscribeClass"},
     *      description="Delete SubscribeClass",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of SubscribeClass",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var SubscribeClass $subscribeClass */
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            return $this->sendErrorWithData(['Subscribe Class not found']);
        }

        $this->subscribeClassRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Subscribe Class deleted successfully');
    }
}
