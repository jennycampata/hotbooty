<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateScheduleDetailAPIRequest;
use App\Http\Requests\Api\UpdateScheduleDetailAPIRequest;
use App\Models\ScheduleDetail;
use App\Repositories\Admin\ScheduleDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class ScheduleDetailController
 * @package App\Http\Controllers\Api
 */

class ScheduleDetailAPIController extends AppBaseController
{
    /** @var  ScheduleDetailRepository */
    private $scheduleDetailRepository;

    public function __construct(ScheduleDetailRepository $scheduleDetailRepo)
    {
        $this->scheduleDetailRepository = $scheduleDetailRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/schedule-details",
     *      summary="Get a listing of the ScheduleDetails.",
     *      tags={"ScheduleDetail"},
     *      description="Get all ScheduleDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ScheduleDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $scheduleDetails = $this->scheduleDetailRepository
            ->pushCriteria(new RequestCriteria($request))
            ->pushCriteria(new LimitOffsetCriteria($request))
            //->pushCriteria(new scheduleDetailCriteria($request))
            ->all();

        return $this->sendResponse($scheduleDetails->toArray(), 'Schedule Details retrieved successfully');
    }

    /**
     * @param CreateScheduleDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/schedule-details",
     *      summary="Store a newly created ScheduleDetail in storage",
     *      tags={"ScheduleDetail"},
     *      description="Store ScheduleDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ScheduleDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ScheduleDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ScheduleDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateScheduleDetailAPIRequest $request)
    {
        $scheduleDetails = $this->scheduleDetailRepository->saveRecord($request);

        return $this->sendResponse($scheduleDetails->toArray(), 'Schedule Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/schedule-details/{id}",
     *      summary="Display the specified ScheduleDetail",
     *      tags={"ScheduleDetail"},
     *      description="Get ScheduleDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ScheduleDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ScheduleDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ScheduleDetail $scheduleDetail */
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            return $this->sendErrorWithData(['Schedule Detail not found']);
        }

        return $this->sendResponse($scheduleDetail->toArray(), 'Schedule Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateScheduleDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/schedule-details/{id}",
     *      summary="Update the specified ScheduleDetail in storage",
     *      tags={"ScheduleDetail"},
     *      description="Update ScheduleDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ScheduleDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ScheduleDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ScheduleDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ScheduleDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateScheduleDetailAPIRequest $request)
    {
        /** @var ScheduleDetail $scheduleDetail */
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            return $this->sendErrorWithData(['Schedule Detail not found']);
        }

        $scheduleDetail = $this->scheduleDetailRepository->updateRecord($request, $scheduleDetail);

        return $this->sendResponse($scheduleDetail->toArray(), 'ScheduleDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/schedule-details/{id}",
     *      summary="Remove the specified ScheduleDetail from storage",
     *      tags={"ScheduleDetail"},
     *      description="Delete ScheduleDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ScheduleDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ScheduleDetail $scheduleDetail */
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            return $this->sendErrorWithData(['Schedule Detail not found']);
        }

        $this->scheduleDetailRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Schedule Detail deleted successfully');
    }
}
