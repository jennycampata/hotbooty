<?php

namespace App\Http\Controllers;

use App\Models\UserClasses;
use App\Repositories\Admin\BookingRepository;
use App\Repositories\Admin\PackageRepository;
use App\Repositories\Admin\ScheduleRepository;
use App\Repositories\Admin\SubscribeClassRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    private $scheduleRepository;
    private $bookingRepository;
    private $subscribeClasses;
    private $packageRepository;

    public function __construct(ScheduleRepository $scheduleRepo, BookingRepository $bookingRepo, SubscribeClassRepository $subscribeRepo, PackageRepository $packageRepo)
    {
        $this->scheduleRepository = $scheduleRepo;
        $this->bookingRepository = $bookingRepo;
        $this->subscribeClasses = $subscribeRepo;
        $this->packageRepository = $packageRepo;
        $this->middleware('auth');
    }

    public function index()
    {
        $id = Auth::id();
        $total = UserClasses::where('user_id', $id)->first();
        $booking = $this->bookingRepository->findWhere(['user_id' => $id]);
        $d = $booking->pluck('schedule_id');
        $date = Carbon::today()->toDateString();
        $schedule = $this->scheduleRepository->orderBy('date', 'asc')->findWhereIn('id', $d->toArray())->where('date', '>=', $date);
        $pschedule = $this->scheduleRepository->orderBy('date', 'asc')->findWhereIn('id', $d->toArray())->where('date', '<', $date);
//        $schedule = $schedules->pagination(7);
        return view('profile')->with([
            'total'     => $total,
            'booking'   => $booking->count(),
            'schedule'  => $schedule,
            'pschedule' => $pschedule
        ]);
    }
}
