<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\ScheduleDetailDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateScheduleDetailRequest;
use App\Http\Requests\Admin\UpdateScheduleDetailRequest;
use App\Repositories\Admin\ScheduleDetailRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class ScheduleDetailController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  ScheduleDetailRepository */
    private $scheduleDetailRepository;

    public function __construct(ScheduleDetailRepository $scheduleDetailRepo)
    {
        $this->scheduleDetailRepository = $scheduleDetailRepo;
        $this->ModelName = 'schedule-details';
        $this->BreadCrumbName = 'Schedule Details';
    }

    /**
     * Display a listing of the ScheduleDetail.
     *
     * @param ScheduleDetailDataTable $scheduleDetailDataTable
     * @return Response
     */
    public function index(ScheduleDetailDataTable $scheduleDetailDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $scheduleDetailDataTable->render('admin.schedule_details.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new ScheduleDetail.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.schedule_details.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created ScheduleDetail in storage.
     *
     * @param CreateScheduleDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateScheduleDetailRequest $request)
    {
        $scheduleDetail = $this->scheduleDetailRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.schedule-details.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.schedule-details.edit', $scheduleDetail->id));
        } else {
            $redirect_to = redirect(route('admin.schedule-details.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified ScheduleDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedule-details.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $scheduleDetail);
        return view('admin.schedule_details.show')->with(['scheduleDetail' => $scheduleDetail, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified ScheduleDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedule-details.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $scheduleDetail);
        return view('admin.schedule_details.edit')->with(['scheduleDetail' => $scheduleDetail, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified ScheduleDetail in storage.
     *
     * @param  int              $id
     * @param UpdateScheduleDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateScheduleDetailRequest $request)
    {
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedule-details.index'));
        }

        $scheduleDetail = $this->scheduleDetailRepository->updateRecord($request, $scheduleDetail);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.schedule-details.create'));
        } else {
            $redirect_to = redirect(route('admin.schedule-details.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified ScheduleDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $scheduleDetail = $this->scheduleDetailRepository->findWithoutFail($id);

        if (empty($scheduleDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedule-details.index'));
        }

        $this->scheduleDetailRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.schedule-details.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
