<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\ScheduleDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateScheduleRequest;
use App\Http\Requests\Admin\UpdateScheduleRequest;
use App\Repositories\Admin\DayRepository;
use App\Repositories\Admin\ScheduleRepository;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class ScheduleController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  ScheduleRepository */
    private $scheduleRepository;
    private $daysRepository;

    public function __construct(ScheduleRepository $scheduleRepo, DayRepository $dayRepo)
    {
        $this->scheduleRepository = $scheduleRepo;
        $this->daysRepository = $dayRepo;
        $this->ModelName = 'schedules';
        $this->BreadCrumbName = 'Schedules';
    }

    /**
     * Display a listing of the Schedule.
     *
     * @param ScheduleDataTable $scheduleDataTable
     * @return Response
     */
    public function index(ScheduleDataTable $scheduleDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $scheduleDataTable->render('admin.schedules.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Schedule.
     *
     * @return Response
     */
    public function create()
    {
        $days = $this->daysRepository->pluck('name');
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.schedules.create')->with([
            'days'  => $days,
            'title' => $this->BreadCrumbName,
        ]);
    }


    /**
     * Store a newly created Schedule in storage.
     *
     * @param CreateScheduleRequest $request
     *
     * @return Response
     */
    public function store(CreateScheduleRequest $request)
    {
//        dd($request->time);

        if ($request->day == 0) {
            $day = $this->getdays($request->day);
        } else if ($request->day == 1) {
            $day = $this->getdays($request->day);
        } else if ($request->day == 2) {
            $day = $this->getdays($request->day);
        } else if ($request->day == 3) {
            $day = $this->getdays($request->day);
        } else if ($request->day == 4) {
            $day = $this->getdays($request->day);
        } else if ($request->day == 5) {
            $day = $this->getdays($request->day);
        } else {
            $day = $this->getdays($request->day);
        }

        foreach ($day as $mon) {
            $request['date'] = $mon;
            $schedule = $this->scheduleRepository->saveRecord($request);
        }

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.schedules.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.schedules.edit', $schedule->id));
        } else {
            $redirect_to = redirect(route('admin.schedules.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    public function getdays($val)
    {

        $dateDay = \Carbon\Carbon::now();//use your date to get month and year
        $year = $dateDay->year;
        $month = $dateDay->month;
        $days = $dateDay->daysInMonth;
        $week = [];
        foreach (range(1, $days) as $day) {
            $date = \Carbon\Carbon::createFromDate($year, $month, $day);
            if ($val == 0) {
                if ($date->isMonday() === true) {
                    $week[] = ($date->toDateString());
                }
            } else if ($val == 1) {
                if ($date->isTuesday() === true) {
                    $week[] = ($date->toDateString());
                }
            } else if ($val == 2) {
                if ($date->isWednesday() === true) {
                    $week[] = ($date->toDateString());
                }
            } else if ($val == 3) {
                if ($date->isThursday() === true) {
                    $week[] = ($date->toDateString());
                }
            } else if ($val == 4) {
                if ($date->isFriday() === true) {
                    $week[] = ($date->toDateString());
                }
            } else if ($val == 5) {
                if ($date->isSaturday() === true) {
                    $week[] = ($date->toDateString());
                }
            } else {
                if ($date->isSunday() === true) {
                    $week[] = ($date->toDateString());
                }
            }


        }
        return $week;
    }

    /**
     * Display the specified Schedule.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $schedule);
        return view('admin.schedules.show')->with(['schedule' => $schedule, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Schedule.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $days = $this->daysRepository->pluck('name');
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $schedule);
        return view('admin.schedules.edit')->with([
            'days'     => $days,
            'schedule' => $schedule,
            'title'    => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Schedule in storage.
     *
     * @param int $id
     * @param UpdateScheduleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateScheduleRequest $request)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        $schedule = $this->scheduleRepository->updateRecord($request, $schedule);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.schedules.create'));
        } else {
            $redirect_to = redirect(route('admin.schedules.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Schedule from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        $this->scheduleRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.schedules.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
