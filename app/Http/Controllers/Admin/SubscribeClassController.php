<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\SubscribeClassDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateSubscribeClassRequest;
use App\Http\Requests\Admin\UpdateSubscribeClassRequest;
use App\Repositories\Admin\SubscribeClassRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class SubscribeClassController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  SubscribeClassRepository */
    private $subscribeClassRepository;

    public function __construct(SubscribeClassRepository $subscribeClassRepo)
    {
        $this->subscribeClassRepository = $subscribeClassRepo;
        $this->ModelName = 'subscribe-classes';
        $this->BreadCrumbName = 'Subscribe Classes';
    }

    /**
     * Display a listing of the SubscribeClass.
     *
     * @param SubscribeClassDataTable $subscribeClassDataTable
     * @return Response
     */
    public function index(SubscribeClassDataTable $subscribeClassDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $subscribeClassDataTable->render('admin.subscribe_classes.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new SubscribeClass.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.subscribe_classes.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created SubscribeClass in storage.
     *
     * @param CreateSubscribeClassRequest $request
     *
     * @return Response
     */
    public function store(CreateSubscribeClassRequest $request)
    {
        $subscribeClass = $this->subscribeClassRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.subscribe-classes.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.subscribe-classes.edit', $subscribeClass->id));
        } else {
            $redirect_to = redirect(route('admin.subscribe-classes.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified SubscribeClass.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.subscribe-classes.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $subscribeClass);
        return view('admin.subscribe_classes.show')->with(['subscribeClass' => $subscribeClass, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified SubscribeClass.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.subscribe-classes.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $subscribeClass);
        return view('admin.subscribe_classes.edit')->with(['subscribeClass' => $subscribeClass, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified SubscribeClass in storage.
     *
     * @param  int              $id
     * @param UpdateSubscribeClassRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubscribeClassRequest $request)
    {
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.subscribe-classes.index'));
        }

        $subscribeClass = $this->subscribeClassRepository->updateRecord($request, $subscribeClass);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.subscribe-classes.create'));
        } else {
            $redirect_to = redirect(route('admin.subscribe-classes.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified SubscribeClass from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subscribeClass = $this->subscribeClassRepository->findWithoutFail($id);

        if (empty($subscribeClass)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.subscribe-classes.index'));
        }

        $this->subscribeClassRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.subscribe-classes.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
