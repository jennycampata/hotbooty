<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\BookingDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBookingRequest;
use App\Http\Requests\Admin\UpdateBookingRequest;
use App\Repositories\Admin\BookingRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class BookingController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  BookingRepository */
    private $bookingRepository;

    public function __construct(BookingRepository $bookingRepo)
    {
        $this->bookingRepository = $bookingRepo;
        $this->ModelName = 'bookings';
        $this->BreadCrumbName = 'Bookings';
    }

    /**
     * Display a listing of the Booking.
     *
     * @param BookingDataTable $bookingDataTable
     * @return Response
     */
    public function index(BookingDataTable $bookingDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $bookingDataTable->render('admin.bookings.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Booking.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.bookings.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Booking in storage.
     *
     * @param CreateBookingRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingRequest $request)
    {
        $booking = $this->bookingRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.bookings.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.bookings.edit', $booking->id));
        } else {
            $redirect_to = redirect(route('admin.bookings.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Booking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $booking);
        return view('admin.bookings.show')->with(['booking' => $booking, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Booking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $booking);
        return view('admin.bookings.edit')->with(['booking' => $booking, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Booking in storage.
     *
     * @param  int              $id
     * @param UpdateBookingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingRequest $request)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        $booking = $this->bookingRepository->updateRecord($request, $booking);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.bookings.create'));
        } else {
            $redirect_to = redirect(route('admin.bookings.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Booking from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        $this->bookingRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.bookings.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
