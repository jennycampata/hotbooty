<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\DayDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateDayRequest;
use App\Http\Requests\Admin\UpdateDayRequest;
use App\Repositories\Admin\DayRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class DayController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  DayRepository */
    private $dayRepository;

    public function __construct(DayRepository $dayRepo)
    {
        $this->dayRepository = $dayRepo;
        $this->ModelName = 'days';
        $this->BreadCrumbName = 'Days';
    }

    /**
     * Display a listing of the Day.
     *
     * @param DayDataTable $dayDataTable
     * @return Response
     */
    public function index(DayDataTable $dayDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $dayDataTable->render('admin.days.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Day.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.days.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Day in storage.
     *
     * @param CreateDayRequest $request
     *
     * @return Response
     */
    public function store(CreateDayRequest $request)
    {
        $day = $this->dayRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.days.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.days.edit', $day->id));
        } else {
            $redirect_to = redirect(route('admin.days.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Day.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $day = $this->dayRepository->findWithoutFail($id);

        if (empty($day)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.days.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $day);
        return view('admin.days.show')->with(['day' => $day, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Day.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $day = $this->dayRepository->findWithoutFail($id);

        if (empty($day)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.days.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $day);
        return view('admin.days.edit')->with(['day' => $day, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Day in storage.
     *
     * @param  int              $id
     * @param UpdateDayRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDayRequest $request)
    {
        $day = $this->dayRepository->findWithoutFail($id);

        if (empty($day)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.days.index'));
        }

        $day = $this->dayRepository->updateRecord($request, $day);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.days.create'));
        } else {
            $redirect_to = redirect(route('admin.days.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Day from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $day = $this->dayRepository->findWithoutFail($id);

        if (empty($day)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.days.index'));
        }

        $this->dayRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.days.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
