<?php

namespace App\Http\Controllers;

use App\Models\UserClasses;
use App\Repositories\Admin\PackageRepository;
use App\Repositories\Admin\SubscribeClassRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;
use Stripe;

class StripeController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */

    private $subscribeClassesRepository;
    private $packageRepository;

    public function __construct(SubscribeClassRepository $subscribeClassesRepo, PackageRepository $packageRepo)
    {
        $this->subscribeClassesRepository = $subscribeClassesRepo;
        $this->packageRepository = $packageRepo;
    }

    public function stripe(Request $request)
    {
        $amount = $request->amount;
        $id = $request->id;
        return view('stripe')->with([
            'amount' => $amount,
            'id'    => $id
        ]);
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        $price = intval($request->amount) * 100;
//        dd($request);

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

//        $stripeToken = \Stripe\Token::create(array(
//            "card" => array(
//                "number" => "4242424242424242",
//                "exp_month" => 1,
//                "exp_year" => 2021,
//                "cvc" => "314"
//            )
//        ));
//        dd($stripeToken->id);

        $charge = Stripe\Charge::create([
            "amount"      => $price,
            "currency"    => "USD",
            "source"      => $request->stripeToken,
            "description" => $request->id
        ]);

        if ($charge) {
            $output = [];
            $output['user_id'] = Auth::id();
            $output['payid'] = $request->stripeToken;

            DB::table('transactions')->insert($output);

            $data = [];
            $data['class_id'] = $request->id;
            $data['user_id'] = Auth::id();
            $this->subscribeClassesRepository->saveRecord($data);
            $package = $this->packageRepository->find($request->id);
            $existClass = UserClasses::where('user_id', Auth::id())->get();
            if(count($existClass) > 0){
                $total['total'] = $existClass[0]->total + $package->number;
                UserClasses::where('user_id', Auth::id())->update($total);
            }

            Session::flash('success', 'Payment successful!');

            return Redirect::to('/profile');
        }
    }
}