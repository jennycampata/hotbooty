<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserClasses extends Model
{
    use SoftDeletes;

    public $table = 'user_classes';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'user_id',
        'total',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'      => 'integer',
        'user_id' => 'integer',
        'total'   => 'integer',

    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'integer',
        'total'   => 'integer',
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'user_id' => 'integer',
        'total'   => 'integer',
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'user_id' => 'integer',
        'total'   => 'integer',
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'user_id' => 'integer',
        'total'   => 'integer',
    ];

    public function days()
    {
        return $this->belongsTo(Day::class, 'day');
    }

    public function detail()
    {
        return $this->hasOne(ScheduleDetail::class, 'schedule_id');
    }
}
