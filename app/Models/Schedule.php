<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Schedule",
 *      required={"id", "day", "type", "time", "equipment", "instructor"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="day",
 *          description="day",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="equipment",
 *          description="equipment",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="instructor",
 *          description="instructor",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Schedule extends Model
{
    use SoftDeletes;

    public $table = 'schedules';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'day',
        'date',
        'type',
        'time',
        'equipment',
        'instructor'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'         => 'integer',
        'day'        => 'string',
        'type'       => 'string',
        'time'       => 'string',
        'equipment'  => 'string',
        'instructor' => 'string'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [
//        'booking'
    ];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'day'        => 'required',
        'type'       => 'required',
        'time'       => 'required',
        'equipment'  => 'required',
        'instructor' => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
//        'id'         => 'required',
        'day'        => 'required',
        'type'       => 'required',
        'time'       => 'required',
        'equipment'  => 'required',
        'instructor' => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'id'         => 'required',
        'day'        => 'required',
        'type'       => 'required',
        'time'       => 'required',
        'equipment'  => 'required',
        'instructor' => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'id'         => 'required',
        'day'        => 'required',
        'type'       => 'required',
        'time'       => 'required',
        'equipment'  => 'required',
        'instructor' => 'required'
    ];

    public function days()
    {
        return $this->belongsTo(Day::class, 'day');
    }

    public function detail()
    {
        return $this->hasOne(ScheduleDetail::class, 'schedule_id');
    }

    public function booking()
    {
        return $this->hasMany(Booking::class, 'schedule_id');
    }
}
